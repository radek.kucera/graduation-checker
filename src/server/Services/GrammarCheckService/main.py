from core import GrammarChecker
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/api/grammar', methods=['GET', 'POST'])
def checkerApi():
    if request.method == 'POST':
        if not request.json or 'text' not in request.json:
            return jsonify({"status": "Missing argument text!"}), 200
        checker = GrammarChecker(request.json["text"])
        return jsonify(checker.get_checked_data()), 200
    return jsonify({"status": "running"}), 200


if __name__ == "__main__":
    app.run(debug=True, port=8000)
