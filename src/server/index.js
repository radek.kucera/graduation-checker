const express = require('express');

const app = express();

app.use(express.static('dist'));

app.get('/api', (req, res) => {
  res.send({ Hello: 'World!' });
});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
