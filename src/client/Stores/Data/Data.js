import initialState from "./DataInitialState";
import CONS from "./DataConstants";

const reducer = (state, action) => {
  state = state || initialState;
  switch (action.type) {
    case CONS.SENDREQUEST_SUCCESS: {
      return { error: null, busy: false, userData: action.payload };
    }
    case CONS.SENDREQUEST_STARTED: {
      return { error: null, busy: true, userData: {} };
    }
    case CONS.SENDREQUEST_FAILURE: {
      return { error: action.payload, busy: false, userData: {} };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
