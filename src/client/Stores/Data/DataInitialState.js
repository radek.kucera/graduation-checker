const initialState = {
  error: null,
  busy: false,
  userData: {}
};

export default initialState;
