import CONS from "./DataConstants";
import axios from "axios";

const dataActionCreators = {
  sendRequest: () => dispatch => {
    dispatch({ type: CONS.SENDREQUEST_STARTED });
    axios
      .get(CONS.URL, {})
      .then(res => {
        dispatch({ type: CONS.SENDREQUEST_SUCCESS, payload: res.data });
      })
      .catch(error => {
        dispatch({ type: CONS.SENDREQUEST_FAILURE, payload: error.response });
      });
  }
};

export default dataActionCreators;
