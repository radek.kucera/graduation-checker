import { combineReducers } from "redux";
import Data from "./Data/Data";

const rootReducer = () => {
  return combineReducers({
    Data: Data
  });
};

export default rootReducer;
