import { applyMiddleware, compose, createStore } from "redux";
import rootReducer from "./rootReducer";
import middle from "./middle";

export default function configureStore(initialState) {
  const middleware = [middle];
  const enhancers = [];

  return createStore(
    rootReducer(),
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );
}
