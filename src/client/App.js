import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import dataActionCreators from "./Stores/Data/DataActions";

class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.sendRequest();
  }

  render() {
    return <h1></h1>;
  }
}

export default connect(
  state => {
    return {
      Data: state.Data
    };
  },
  {
    sendRequest: dataActionCreators.sendRequest
  }
)(App);
